$(function () {
    var APPLICATION_ID = "25D7343E-D258-C34D-FF02-6FA8C67C4300",
    SECRET_KEY = "9C33A9F7-BC24-D3C6-FFB6-766B54903900",
    VERSION = "v1";
    
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var user = new Backendless.User();
    user.email = "annagivensphsxoxo@gmail.com";
    user.password = "password";
    Backendless.UserService.register(user); 

    
    var postsCollection = Backendless.Persistence.of(Posts).find();
 
     console.log(postsCollection);
     
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function(time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper); 
    
    $('.main-container').html(blogHTML);
    
}); 

function Posts (args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
} 